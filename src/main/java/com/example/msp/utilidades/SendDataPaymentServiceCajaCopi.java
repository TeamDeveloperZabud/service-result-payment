/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.utilidades;

import com.example.msp.model.AuthenticateToken.model.AuthenticateToken;
import com.example.msp.model.AuthenticateToken.service.AuthenticateTokenService;
import com.example.msp.model.InformationTransaction.model.InformationTransaction;
import com.example.msp.model.InformationTransaction.resource.dto.InformationTransactionDTO;
import com.example.msp.model.InformationTransaction.service.InformationTransactionService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author DELL - ZABUD
 */
@Component
public class SendDataPaymentServiceCajaCopi {

    @Autowired
    private InformationTransactionService InformationTransactionService;

    @Autowired
    private AuthenticateTokenService authenticateTokenService;

    ObjectMapper objectMapper = ObjectMapperUtil.getInstanceObjectMapper();

    @Scheduled(fixedRate = 300000)
    public void validSendDataPaymentServiceCajaCopi() throws JsonProcessingException, IOException {
        List<InformationTransaction> listSendDataPending = InformationTransactionService.findBySendData();
        for (InformationTransaction item : listSendDataPending) {
            //Se consulta información de la authenticación con el cus para enviarla al servidor de caja copi si proviene de allá
            AuthenticateToken objConcetPayment = authenticateTokenService.findByCus(item.getCus());

            if (objConcetPayment.getIdConcetPayment().equals("1")) {
                item.setSendData(Boolean.TRUE);
                InformationTransactionService.updateStatusSendData(item.getId(), Boolean.TRUE);
                authenticateTokenService.UpdateStatusSendData(objConcetPayment.getId(), Boolean.TRUE);
            } else if (objConcetPayment.getIdConcetPayment().equals("2")) {
                callAVGTransaction(item, Constants.URL_SERVICE_CAJACOPI, objConcetPayment);
            } else if (objConcetPayment.getIdConcetPayment().equals("3")) {
                callAVGTransaction(item, Constants.URL_SERVICE_CAJACOPI_RESBAR, objConcetPayment);
            }

        }
    }

    public void callAVGTransaction(InformationTransaction item, String url, AuthenticateToken objConcetPayment) throws JsonProcessingException, IOException {
        //Se envia información de authenticación de la transacción
        String POST_PARAMS_AUTHENTICATE = objectMapper.writeValueAsString(item);
        String data_authenticate = CallServiceExternal.POST((url + "AVGTransactionAuth/AuthenticateToken"), POST_PARAMS_AUTHENTICATE);
        ResultServiceExternalDTO resultSendDataAuthenticate = this.objectMapper.readValue(data_authenticate, ResultServiceExternalDTO.class);
        //Se valida si se realizo el envio de la información correctamente
        if (resultSendDataAuthenticate.getAuthenticate()) {

            //Se actualiza el estado del envio y se envia la información de la transacción
//            authenticateTokenService.UpdateStatusSendData(objConcetPayment.getId(), Boolean.TRUE);
//            String POST_PARAMS_INFORMATION = objectMapper.writeValueAsString(objConcetPayment);
//            String data_information = CallServiceExternal.POST((url + "AVGTransaction/InformationTransaction"), POST_PARAMS_INFORMATION);
//            ResultServiceExternalDTO resultSendDataInformation = this.objectMapper.readValue(data_information, ResultServiceExternalDTO.class);
//
//            if (resultSendDataInformation.getTransactionSaved()) {
//                //Se actualiza estado del envio de la información
//                InformationTransactionService.updateStatusSendData(objConcetPayment.getId(), Boolean.TRUE);
//            }
        }
    }

}
