/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.utilidades;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DADUIS
 */
public class CallServiceExternal {

    private final static Logger LOGGER = Logger.getLogger("CallServiceExternal");

    /**
     *
     * @param url
     * @param params
     * @param content_type
     * @return
     */
    public static String POST(String url, String params) {
        try {
            StringBuffer response = new StringBuffer();
            URL obj = new URL(url);

            HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();
            postConnection.setRequestMethod("POST");
            postConnection.setRequestProperty("Content-Type", "application/json");
            postConnection.setDoOutput(true);
            OutputStream os = postConnection.getOutputStream();
            os.write(params.getBytes());
            os.flush();
            os.close();
            int responseCode = postConnection.getResponseCode();
            LOGGER.log(Level.INFO, "Respuesta: {0}", responseCode);
            LOGGER.log(Level.INFO, "Respuesta: {0}", postConnection.getResponseMessage());

            if (responseCode == HttpURLConnection.HTTP_OK) { //success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        postConnection.getInputStream()));
                String inputLine;
                response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                // print result
                LOGGER.log(Level.INFO, "Respuesta: {0}", response.toString());
            } else {
                LOGGER.log(Level.WARNING, "POST NO FUNCIONÓ");
            }

            return response.toString();
        } catch (Exception ex) {
            return "{\"Error\":\"\",\"CodeError\":1,\"Authenticate\":false}";
        }
    }
}
