package com.example.msp.utilidades;

public interface Constants {
	static final String GET_USER_BY_ID = "/getUser/{userId}";
	static final String GET_ALL_USERS = "/getAllUsers";
	static final String SAVE_USER = "/saveUser";
	static final String ALLOWED_ORIGINS = "*";
        static final String URL_SERVICE_CAJACOPI = "http://190.131.209.90:5230/";
        static final String URL_SERVICE_CAJACOPI_RESBAR = "http://190.131.209.90:5230/";
//        static final String URL_SERVICE_CAJACOPI = "http://localhost:55909/";
}
