/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.utilidades;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author DADUIS
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResultServiceExternalDTO {
    
    @JsonProperty("Error")
    private String Error;
    @JsonProperty("CodeError")
    private int CodeError;
    @JsonProperty("Authenticate")
    private Boolean Authenticate;
    @JsonProperty("TransactionSaved")
    private Boolean TransactionSaved;

    public ResultServiceExternalDTO(@JsonProperty("Error") String Error, @JsonProperty("CodeError") int CodeError, @JsonProperty("Authenticate") Boolean Authenticate, @JsonProperty("TransactionSaved") Boolean TransactionSaved) {
        this.Error = Error;
        this.CodeError = CodeError;
        this.Authenticate = Authenticate;
        this.TransactionSaved = TransactionSaved;
    }
    
    public String getError() {
        return Error;
    }

    public void setError(String Error) {
        this.Error = Error;
    }

    public int getCodeError() {
        return CodeError;
    }

    public void setCodeError(int CodeError) {
        this.CodeError = CodeError;
    }

    public Boolean getAuthenticate() {
        return Authenticate;
    }

    public void setAuthenticate(Boolean Authenticate) {
        this.Authenticate = Authenticate;
    }
    
    public Boolean getTransactionSaved() {
        return TransactionSaved;
    }

    public void setTransactionSaved(Boolean TransactionSaved) {
        this.TransactionSaved = TransactionSaved;
    }
}
