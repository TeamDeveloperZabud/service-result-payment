/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.AuthenticateToken.service;

import com.example.msp.model.AuthenticateToken.model.AuthenticateToken;
import com.example.msp.model.AuthenticateToken.resource.dto.AuthenticateTokenDTO;

/**
 *
 * @author DADUIS
 */
public interface AuthenticateTokenService {

    public void guardarAuthenticateToken(AuthenticateTokenDTO dto);

    AuthenticateToken findByCus(Long cus);
    
    void UpdateStatusSendData(Long Id, Boolean Status);
}
