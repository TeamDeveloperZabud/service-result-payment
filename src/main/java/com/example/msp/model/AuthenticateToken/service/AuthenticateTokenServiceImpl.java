/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.AuthenticateToken.service;

import com.example.msp.model.AuthenticateToken.model.AuthenticateToken;
import com.example.msp.model.AuthenticateToken.model.AuthenticateTokenRepository;
import com.example.msp.model.AuthenticateToken.resource.dto.AuthenticateTokenDTO;
import com.example.msp.utilidades.ObjectMapperUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 *
 * @author DADUIS
 */
@Service
public class AuthenticateTokenServiceImpl implements AuthenticateTokenService {

    @Autowired
    private AuthenticateTokenRepository AuthenticateTokenRepository;
    ObjectMapper objectMapper = ObjectMapperUtil.getInstanceObjectMapper();

    @Override
    public void guardarAuthenticateToken(AuthenticateTokenDTO dto) {
        AuthenticateToken dtoSave = objectMapper.convertValue(dto, AuthenticateToken.class);
        AuthenticateTokenRepository.save(dtoSave);
    }

    @Override
    public AuthenticateToken findByCus(Long cus) {
        return AuthenticateTokenRepository.findByCusM(cus);
    }

    public void UpdateStatusSendData(Long Id, Boolean Status) {
        AuthenticateTokenRepository.UpdateStatusSendData(Id, Status);
    }
}
