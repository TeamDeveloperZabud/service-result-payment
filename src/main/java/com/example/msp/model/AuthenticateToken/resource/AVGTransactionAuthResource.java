/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.AuthenticateToken.resource;

import com.example.msp.model.AuthenticateToken.resource.dto.AuthenticateTokenDTO;
import com.example.msp.model.AuthenticateToken.service.AuthenticateTokenService;
import com.example.msp.utilidades.ObjectMapperUtil;
import com.example.msp.model.AuthenticateToken.resource.dto.ResultDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author DADUIS
 */
@RestController
@RequestMapping("/AVGTransactionAuth")
public class AVGTransactionAuthResource {

    @Autowired
    private AuthenticateTokenService AuthenticateTokenService;
    ObjectMapper objectMapper = ObjectMapperUtil.getInstanceObjectMapper();

    @RequestMapping(value = "AuthenticateToken", method = RequestMethod.POST)
    public ResultDTO AuthenticateToken(HttpEntity<String> httpEntity /*@RequestBody TestDTO dto @PathVariable String generado*/) throws IOException {
        ResultDTO ObjResult = new ResultDTO("", 0, Boolean.TRUE);
        try {
            String json = httpEntity.getBody();
            AuthenticateTokenDTO dto = objectMapper.readValue(json, AuthenticateTokenDTO.class);
            AuthenticateTokenService.guardarAuthenticateToken(dto);
        } catch (Exception ex) {
            ObjResult = new ResultDTO("Error al guardar autenticación.", 1, Boolean.FALSE);
        }
        return ObjResult;
    }
}
