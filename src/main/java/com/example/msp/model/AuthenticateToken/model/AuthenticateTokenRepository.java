/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.AuthenticateToken.model;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author DADUIS
 */
public interface AuthenticateTokenRepository extends CrudRepository<AuthenticateToken, Long> {

    @Query(value = "select * from ps_authenticate_token aut where aut.cus = :Cus limit 1", nativeQuery = true)
    AuthenticateToken findByCusM(@Param("Cus") Long Cus);
    
    @Transactional
    @Modifying
    @Query(value = "update ps_authenticate_token set send_data= :send_data  where ID = :Id", nativeQuery = true)
    void UpdateStatusSendData(@Param("Id") Long id, @Param("send_data") Boolean send_data);
}
