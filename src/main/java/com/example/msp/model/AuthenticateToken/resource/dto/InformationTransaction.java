/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.AuthenticateToken.resource.dto;

import java.util.Date;


/**
 *
 * @author DADUIS
 */
public class InformationTransaction {
    private Long id;
    private String UserAVG;
    private String PasswordAVG;
    private Long Cus;
    private String StatusTransaction;
    private Date bankProcessDate;
    private Date SoliciteDate;
    private String ClientUrl;
    private int TransactionCycle;
    private int VatValue;
    private Long Amount;
    private String InvoiceDescription;
    private String BankName;
}
