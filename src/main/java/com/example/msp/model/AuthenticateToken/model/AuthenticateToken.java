/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.AuthenticateToken.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

/**
 *
 * @author DADUIS
 */
@Entity
@Table(name = "ps_authenticate_token")
public class AuthenticateToken {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @javax.persistence.Id
    private Long id;
    private String UserAVG;
    private String PasswordAVG;
    private Long Cus;
    private Long IdTicket;
    private String IdConceptPayment;
    private Long idUserCustomerClient;
    private Boolean SendData;

    public AuthenticateToken() {
    }

    public AuthenticateToken(Long id, String UserAVG, String PasswordAVG, Long Cus, Long IdTicket, String IdConceptPayment, Long idUserCustomerClient, Boolean SendData) {
        this.id = id;
        this.UserAVG = UserAVG;
        this.PasswordAVG = PasswordAVG;
        this.Cus = Cus;
        this.IdTicket = IdTicket;
        this.IdConceptPayment = IdConceptPayment;
        this.idUserCustomerClient = idUserCustomerClient;
        this.SendData = SendData;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long _id) {
        this.id = _id;
    }

    public String getUserAvg() {
        return this.UserAVG;
    }

    public void setUserAvg(String _useravg) {
        this.UserAVG = _useravg;
    }

    public String getPasswordAvg() {
        return this.PasswordAVG;
    }

    public void setPasswordAvg(String _passwordavg) {
        this.PasswordAVG = _passwordavg;
    }

    public Long getCus() {
        return this.Cus;
    }

    public void setCusAvg(Long _cus) {
        this.Cus = _cus;
    }

    public Long getIdTicket() {
        return this.IdTicket;
    }

    public void setIdTicket(Long _idticket) {
        this.IdTicket = _idticket;
    }

    public String getIdConcetPayment() {
        return this.IdConceptPayment;
    }

    public void setIdConcetPayment(String _idconcetpayment) {
        this.IdConceptPayment = _idconcetpayment;
    }

    public Long getIdCustomerClient() {
        return this.idUserCustomerClient;
    }

    public void setIdCustomerClient(Long _idusercustomerclient) {
        this.idUserCustomerClient = _idusercustomerclient;
    }
    
    public Boolean getSendData() {
        return this.SendData;
    }

    public void setSendData(Boolean SendData) {
        this.SendData = SendData;
    }
}
