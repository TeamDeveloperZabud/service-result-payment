/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.ChatBot.service;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mapog
 */
public interface MessageService {

    ArrayList findWelcomeButtons();
    ArrayList findConfigurationUserButtons();
    ArrayList findProductButtons();
    Long saveUnknowWords(String unknowWord);
    void sendEmail(String unknowWord);
}
