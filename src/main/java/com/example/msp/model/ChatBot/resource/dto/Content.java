/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.ChatBot.resource.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author mapog
 */
@JsonInclude(Include.NON_NULL)
public class Content {

    Attachment attachment = null;
    
    private String text = "";
    

    /**
     *
     * @param text
     * @param attachment
     */
    public Content(@JsonProperty("text") String text, @JsonProperty("attachment") Attachment attachment) {
        this.text = text;
        this.attachment = attachment;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Attachment getAttachment() {
        return attachment;
    }

    public void setAttachment(Attachment attachment) {
        this.attachment = attachment;
    }

}
