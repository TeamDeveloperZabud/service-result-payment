/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.ChatBot.service;

import com.example.msp.model.ChatBot.model.MessageRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;
import org.springframework.mail.javamail.JavaMailSender;

/**
 *
 * @author mapog
 */
@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private JavaMailSender javaMailSender;

    @Override
    public void sendEmail(String unknowWord) {

        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo("mapoguti@gmail.com");
        msg.setSubject("New Unknow Word");
        msg.setText("There is a new word: \n" + unknowWord);

        javaMailSender.send(msg);

    }

    @Override
    public ArrayList findWelcomeButtons() {
        return messageRepository.findWelcomeButtons();
    }

    @Override
    public ArrayList findConfigurationUserButtons() {
        return messageRepository.findConfigurationUserButtons();
    }

    @Override
    public ArrayList findProductButtons() {
        return messageRepository.findProductButtons();
    }

    @Override
    public Long saveUnknowWords(String unknowWord) {
        return messageRepository.saveUnknowWords(unknowWord);
    }

}
