/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.ChatBot.resource.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mapog
 */
public class ChatResponseDTO implements Serializable {
    List<Content> messages ;

    public List<Content> getMessages() {
        return messages;
    }

    public void setMessages(List<Content> messages) {
        this.messages = messages;
    }
    
}
