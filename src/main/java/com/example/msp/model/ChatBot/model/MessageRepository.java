/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.ChatBot.model;

import java.util.ArrayList;
import org.hibernate.annotations.Nationalized;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author mapog
 */
public interface MessageRepository extends CrudRepository<Message, Long> {

   
    
    
    @Nationalized
    @Query(value = "SELECT id_cb_message, description, json, id_message_type FROM molastore.cb_message where id_message_type= 5;", nativeQuery = true)
    ArrayList findWelcomeButtons();

    @Nationalized
    @Query(value = "SELECT id_cb_message, description, json, id_message_type FROM molastore.cb_message where id_message_type= 6;", nativeQuery = true)
    ArrayList findConfigurationUserButtons();

    @Nationalized
    @Query(value = "SELECT id_cb_message, description, json, id_message_type FROM molastore.cb_message where id_message_type= 7;", nativeQuery = true)
    ArrayList findProductButtons();

    @Nationalized
    @Transactional
    @Query(value = "call CB_saveUknowWord(:unknowWord)", nativeQuery = true)
    Long saveUnknowWords(@Param("unknowWord") String unknowWord);

    
  

}
