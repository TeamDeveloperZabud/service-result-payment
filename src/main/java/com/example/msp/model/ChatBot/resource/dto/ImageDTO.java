/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.ChatBot.resource.dto;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author mapog
 */
@Entity
@Table(name = "ps_image")
public class ImageDTO {

    private Long id_product;
    @Id 
    private Long id_image;
    public ImageDTO(){
    }
    public ImageDTO(Long id_product, Long id_image) {
        this.id_product = id_product;
        this.id_image = id_image;
    }

    public Long getId_product() {
        return id_product;
    }

    public void setId_product(Long id_product) {
        this.id_product = id_product;
    }

    public Long getId_image() {
        return id_image;
    }

    public void setId_image(Long id_image) {
        this.id_image = id_image;
    }
    
}
