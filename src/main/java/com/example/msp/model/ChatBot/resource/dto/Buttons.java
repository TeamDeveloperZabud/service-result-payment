/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.ChatBot.resource.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author mapog
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Buttons {
    
    private String type;
    private List<String> block_names;
    private String url;
    private String title;

    public Buttons(@JsonProperty("type") String type, @JsonProperty("block_names") List<String> block_names, @JsonProperty("title") String title,@JsonProperty("url") String url) {
        this.type = type;
        this.block_names = block_names;
        this.title = title;
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getBlock_names() {
        return block_names;
    }

    public void setBlock_names(List<String> block_names) {
        this.block_names = block_names;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
