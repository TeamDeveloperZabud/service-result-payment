/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.ChatBot.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

/**
 *
 * @author mapog
 */
@Entity
@Table(name = "cb_message")
public class Message {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @javax.persistence.Id
    private int id_cb_message;
    private String description;
    private String json;
    private int id_message_type;

    public Message(int id_cb_message) {
    }

    public Message() {
    }

    public Message(int id_cb_message, String description, String json, int id_message_type) {
        this.id_cb_message = id_cb_message;
        this.description = description;
        this.json = json;
        this.id_message_type = id_message_type;
    }

    public int getId_cb_message() {
        return id_cb_message;
    }

    public void setId_cb_message(int id_cb_message) {
        this.id_cb_message = id_cb_message;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public int getId_message_type() {
        return id_message_type;
    }

    public void setId_message_type(int id_message_type) {
        this.id_message_type = id_message_type;
    }

}
