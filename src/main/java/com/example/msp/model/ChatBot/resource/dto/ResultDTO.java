/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.ChatBot.resource.dto;

/**
 *
 * @author mapog
 */
public class ResultDTO {

    private Long id_product;
    private String name;
    private Double price;
    private Long id_image;
    private String Error;
    private int CodeError;

    public ResultDTO(Long id_product, String name, Double price, Long id_image, String Error, int CodeError) {
        this.id_product = id_product;
        this.name = name;
        this.price = price;
        this.id_image = id_image;
        this.Error = Error;
        this.CodeError = CodeError;
    }

    public Long getId_product() {
        return id_product;
    }

    public void setId_product(Long id_product) {
        this.id_product = id_product;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getId_image() {
        return id_image;
    }

    public void setId_image(Long id_image) {
        this.id_image = id_image;
    }

    public String getError() {
        return Error;
    }

    public void setError(String Error) {
        this.Error = Error;
    }

    public int getCodeError() {
        return CodeError;
    }

    public void setCodeError(int CodeError) {
        this.CodeError = CodeError;
    }
    
}
