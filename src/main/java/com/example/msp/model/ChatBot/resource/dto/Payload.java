/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.ChatBot.resource.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author mapog
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Payload {

    private String url;
    private String template_type;
    private String text;
    private List<Buttons> buttons;

    public Payload(@JsonProperty("url") String url, @JsonProperty("template_type") String template_type, @JsonProperty("text") String text, @JsonProperty("buttons") List<Buttons> buttons) {
        this.url = url;
        this.template_type = template_type;
        this.text = text;
        this.buttons = buttons;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTemplate_type() {
        return template_type;
    }

    public void setTemplate_type(String template_type) {
        this.template_type = template_type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Buttons> getButtons() {
        return buttons;
    }

    public void setButtons(List<Buttons> buttons) {
        this.buttons = buttons;
    }
}
