/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.ChatBot.resource;

import com.example.msp.model.AuthenticateToken.resource.dto.AuthenticateTokenDTO;
import com.example.msp.model.AuthenticateToken.resource.dto.ResultDTO;
import com.example.msp.model.ChatBot.model.Message;
import com.example.msp.model.ChatBot.resource.dto.ChatResponseDTO;
import com.example.msp.model.ChatBot.resource.dto.Content;
import com.example.msp.model.ChatBot.service.MessageService;
import com.example.msp.utilidades.ObjectMapperUtil;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author mapog
 */
@RestController
@RequestMapping("/ChatBot")
public class ChatBotResource {

    @Autowired
    private MessageService messageService;
    ObjectMapper objectMapper = ObjectMapperUtil.getInstanceObjectMapper();

    @RequestMapping(value = "findWelcomeButtons", method = RequestMethod.GET)
    public ChatResponseDTO findWelcomeButtons() {
        ChatResponseDTO ObjResult = new ChatResponseDTO();
        List<Content> lcp = new ArrayList<>();
        try {
            ArrayList response = messageService.findWelcomeButtons();
            for (int i = 0; i < response.size(); i++) {
                Object[] o = (Object[]) response.get(i);
                String objeto = o.toString();
                Message m = new Message((int) o[0], o[1].toString(), o[2].toString(), (int) o[3]);
                //Message m = new Message(new Integer(o[0].toString()), o[1].toString(), o[2].toString(), new Integer(o[3].toString()));

                lcp.add(objectMapper.readValue(m.getJson(), Content.class));
                System.out.println(lcp);
            }
            ObjResult.setMessages(lcp);
            return ObjResult;

        } catch (Exception ex) {
            ObjResult = new ChatResponseDTO();
        }
        return null;

    }

    @RequestMapping(value = "findConfigurationUserButtons", method = RequestMethod.GET)
    public ChatResponseDTO findConfigurationUserButtons() {
        ChatResponseDTO ObjResult = new ChatResponseDTO();
        List<Content> lcp = new ArrayList<>();
        try {
            ArrayList response = messageService.findConfigurationUserButtons();
            for (int i = 0; i < response.size(); i++) {
                Object[] o = (Object[]) response.get(i);
                String objeto = o.toString();
                Message m = new Message((int) o[0], o[1].toString(), o[2].toString(), (int) o[3]);
                //Message m = new Message(new Integer(o[0].toString()), o[1].toString(), o[2].toString(), new Integer(o[3].toString()));

                lcp.add(objectMapper.readValue(m.getJson(), Content.class));
                System.out.println(lcp);
            }
            ObjResult.setMessages(lcp);
            return ObjResult;

        } catch (Exception ex) {
            ObjResult = new ChatResponseDTO();
        }
        return null;

    }

    @RequestMapping(value = "findProductButtons", method = RequestMethod.GET)
    public ChatResponseDTO findProductButtons() {
        ChatResponseDTO ObjResult = new ChatResponseDTO();
        List<Content> lcp = new ArrayList<>();
        try {
            ArrayList response = messageService.findProductButtons();
            for (int i = 0; i < response.size(); i++) {
                Object[] o = (Object[]) response.get(i);
                String objeto = o.toString();
                Message m = new Message((int) o[0], o[1].toString(), o[2].toString(), (int) o[3]);
                //Message m = new Message(new Integer(o[0].toString()), o[1].toString(), o[2].toString(), new Integer(o[3].toString()));

                lcp.add(objectMapper.readValue(m.getJson(), Content.class));
                System.out.println(lcp);
            }
            ObjResult.setMessages(lcp);
            return ObjResult;

        } catch (Exception ex) {
            ObjResult = new ChatResponseDTO();
        }
        return null;

    }

    @RequestMapping(value = "saveUnknowWord/{unknowWord}", method = RequestMethod.GET)
    public ChatResponseDTO saveUnknowWord(@PathVariable String unknowWord /*@RequestBody TestDTO dto @PathVariable String generado*/) throws IOException {
        ChatResponseDTO ObjResult = new ChatResponseDTO();
        String out = "";
        List<Content> lcp = new ArrayList<>();
        lcp.add(new Content("gracias", null));
        ObjResult.setMessages(lcp);
        unknowWord.toLowerCase();
        unknowWord = unknowWord.replace("+", " ");
        try {
            Long response = messageService.saveUnknowWords(unknowWord);
            if (response != 1) {
                return ObjResult;
            } else {
                messageService.sendEmail(unknowWord);
                return ObjResult;
            }
        } catch (Exception ex) {
            System.err.println(ex);
            List<Content> lcp2 = new ArrayList<>();
            lcp2.add(new Content(ex.getMessage(), null));
            ObjResult = new ChatResponseDTO();
            ObjResult.setMessages(lcp2);
            return ObjResult;

        }
    }
}
