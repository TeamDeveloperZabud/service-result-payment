/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.InformationTransaction.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

/**
 *
 * @author DADUIS
 */
@Entity
@Table(name = "ps_information_transation")
public class InformationTransaction {
    
    @GeneratedValue(strategy = GenerationType.AUTO)
    @javax.persistence.Id
    private Long id;
    private String UserAVG;
    private String PasswordAVG;
    private Long Cus;
    private String StatusTransaction;
    private Date bankProcessDate;
    private Date SoliciteDate;
    private String ClientUrl;
    private int TransactionCycle;
    private int VatValue;
    private Long Amount;
    private String InvoiceDescription;
    private String BankName;
    private Boolean SendData;

    public InformationTransaction() {
    }

    public InformationTransaction(Long id, String UserAVG, String PasswordAVG, Long Cus, String StatusTransaction, Date bankProcessDate, Date SoliciteDate, String ClientUrl, int TransactionCycle, int VatValue, Long Amount, String InvoiceDescription, String BankName, Boolean SendData) {
        this.id = id;
        this.UserAVG = UserAVG;
        this.PasswordAVG = PasswordAVG;
        this.Cus = Cus;
        this.StatusTransaction = StatusTransaction;
        this.bankProcessDate = bankProcessDate;
        this.SoliciteDate = SoliciteDate;
        this.ClientUrl = ClientUrl;
        this.TransactionCycle = TransactionCycle;
        this.VatValue = VatValue;
        this.Amount = Amount;
        this.InvoiceDescription = InvoiceDescription;
        this.BankName = BankName;
        this.SendData = SendData;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserAVG() {
        return this.UserAVG;
    }

    public void setUserAVG(String UserAVG) {
        this.UserAVG = UserAVG;
    }

    public String getPasswordAVG() {
        return this.PasswordAVG;
    }

    public void setPasswordAVG(String PasswordAVG) {
        this.PasswordAVG = PasswordAVG;
    }

    public Long getCus() {
        return this.Cus;
    }

    public void setCus(Long Cus) {
        this.Cus = Cus;
    }

    public String getStatusTransaction() {
        return this.StatusTransaction;
    }

    public void setStatusTransaction(String StatusTransaction) {
        this.StatusTransaction = StatusTransaction;
    }

    public Date getBankProcessDate() {
        return this.bankProcessDate;
    }

    public void setBankProcessDate(Date bankProcessDate) {
        this.bankProcessDate = bankProcessDate;
    }

    public Date getSoliciteDate() {
        return this.SoliciteDate;
    }

    public void setSoliciteDate(Date SoliciteDate) {
        this.SoliciteDate = SoliciteDate;
    }

    public String getClientUrl() {
        return this.ClientUrl;
    }

    public void setClientUrl(String ClientUrl) {
        this.ClientUrl = ClientUrl;
    }

    public int getTransactionCycle() {
        return this.TransactionCycle;
    }

    public void setTransactionCycle(int TransactionCycle) {
        this.TransactionCycle = TransactionCycle;
    }

    public int getVatValue() {
        return this.VatValue;
    }

    public void setVatValue(int VatValue) {
        this.VatValue = VatValue;
    }

    public Long getAmount() {
        return this.Amount;
    }

    public void setAmount(Long Amount) {
        this.Amount = Amount;
    }

    public String getInvoiceDescription() {
        return InvoiceDescription;
    }

    public void setInvoiceDescription(String InvoiceDescription) {
        this.InvoiceDescription = InvoiceDescription;
    }

    public String getBankName() {
        return this.BankName;
    }

    public void setBankName(String BankName) {
        this.BankName = BankName;
    }
    
    public Boolean getSendData() {
        return this.SendData;
    }

    public void setSendData(Boolean SendData) {
        this.SendData = SendData;
    }
}
