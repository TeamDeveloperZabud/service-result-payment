/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.InformationTransaction.resource.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author DADUIS
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InformationTransactionDTO {
    private Long id;
    private String UserAVG;
    private String PasswordAVG;
    private Long Cus;
    private String StatusTransaction;
    private String bankProcessDate;
    private String SoliciteDate;
    private String ClientUrl;
    private int TransactionCycle;
    private int VatValue;
    private Long Amount;
    private String InvoiceDescription;
    private String BankName;
    private Boolean SendData;

    public InformationTransactionDTO(@JsonProperty("Cus") Long Cus) {
        this.Cus = Cus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserAVG() {
        return UserAVG;
    }

    public void setUserAVG(String UserAVG) {
        this.UserAVG = UserAVG;
    }

    public String getPasswordAVG() {
        return PasswordAVG;
    }

    public void setPasswordAVG(String PasswordAVG) {
        this.PasswordAVG = PasswordAVG;
    }

    public Long getCus() {
        return Cus;
    }

    public void setCus(Long Cus) {
        this.Cus = Cus;
    }

    public String getStatusTransaction() {
        return StatusTransaction;
    }

    public void setStatusTransaction(String StatusTransaction) {
        this.StatusTransaction = StatusTransaction;
    }

    public String getBankProcessDate() {
        return bankProcessDate;
    }

    public void setBankProcessDate(String bankProcessDate) {
        this.bankProcessDate = bankProcessDate;
    }

    public String getSoliciteDate() {
        return SoliciteDate;
    }

    public void setSoliciteDate(String SoliciteDate) {
        this.SoliciteDate = SoliciteDate;
    }

    public String getClientUrl() {
        return ClientUrl;
    }

    public void setClientUrl(String ClientUrl) {
        this.ClientUrl = ClientUrl;
    }

    public int getTransactionCycle() {
        return TransactionCycle;
    }

    public void setTransactionCycle(int TransactionCycle) {
        this.TransactionCycle = TransactionCycle;
    }

    public int getVatValue() {
        return VatValue;
    }

    public void setVatValue(int VatValue) {
        this.VatValue = VatValue;
    }

    public Long getAmount() {
        return Amount;
    }

    public void setAmount(Long Amount) {
        this.Amount = Amount;
    }

    public String getInvoiceDescription() {
        return InvoiceDescription;
    }

    public void setInvoiceDescription(String InvoiceDescription) {
        this.InvoiceDescription = InvoiceDescription;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String BankName) {
        this.BankName = BankName;
    }
    
    public Boolean getSendData() {
        return SendData;
    }

    public void setSendData(Boolean SendData) {
        this.SendData = SendData;
    }
}
