/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.InformationTransaction.service;

import com.example.msp.model.InformationTransaction.model.InformationTransaction;
import com.example.msp.model.InformationTransaction.resource.dto.InformationTransactionDTO;
import java.util.List;

/**
 *
 * @author DADUIS
 */
public interface InformationTransactionService {
    public InformationTransaction guardarInformationTransaction(InformationTransactionDTO dto);
    public void updateStatusTransaction(Long id,int state);
    public Long GetOrderToUpdate(Long cus);
    public void updateStatusSendData(Long id, Boolean state);
    public List<InformationTransaction> findBySendData();
}
