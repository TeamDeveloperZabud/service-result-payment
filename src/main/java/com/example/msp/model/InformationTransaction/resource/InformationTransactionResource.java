/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.InformationTransaction.resource;

import com.example.msp.model.AuthenticateToken.model.AuthenticateToken;
import com.example.msp.model.AuthenticateToken.service.AuthenticateTokenService;
import com.example.msp.model.InformationTransaction.model.InformationTransaction;
import com.example.msp.model.InformationTransaction.resource.dto.InformationTransactionDTO;
import com.example.msp.model.InformationTransaction.resource.dto.ResultDTO;
import com.example.msp.model.InformationTransaction.service.InformationTransactionService;
import com.example.msp.utilidades.CallServiceExternal;
import com.example.msp.utilidades.ObjectMapperUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.example.msp.utilidades.Constants;
import com.example.msp.utilidades.ResultServiceExternalDTO;

/**
 *
 * @author DADUIS
 */
@RestController
@RequestMapping("/AVGTransaction")
public class InformationTransactionResource {

    @Autowired
    private InformationTransactionService InformationTransactionService;
    @Autowired
    private AuthenticateTokenService authenticateTokenService;

    ObjectMapper objectMapper = ObjectMapperUtil.getInstanceObjectMapper();
    private final static Logger LOGGER = Logger.getLogger("InformationTransactionResource");

    @RequestMapping(value = "InformationTransaction", method = RequestMethod.POST)
    public ResultDTO AuthenticateToken(HttpEntity<String> httpEntity) throws IOException {
        ResultDTO ObjResult = new ResultDTO("", 0, Boolean.TRUE);
        try {
            String json = httpEntity.getBody();
            InformationTransactionDTO dto = objectMapper.readValue(json, InformationTransactionDTO.class);
            InformationTransaction resultInsertInformation = InformationTransactionService.guardarInformationTransaction(dto);
            Long idOrder = InformationTransactionService.GetOrderToUpdate(dto.getCus());
            //Se consulta información de la authenticación con el cus para enviarla al servidor de caja copi si proviene de allá
            AuthenticateToken objConcetPayment = authenticateTokenService.findByCus(dto.getCus());
            //Se verifica si el pago proviene de caja copi o de mola
            if (objConcetPayment.getIdConcetPayment().equals("1")) {
                if (dto.getStatusTransaction().toUpperCase().equalsIgnoreCase("APROBADA")) {
                    InformationTransactionService.updateStatusTransaction(idOrder, 2);
                } else {
                    InformationTransactionService.updateStatusTransaction(idOrder, 8);
                }
            } else if (objConcetPayment.getIdConcetPayment().equals("2")) {
                //Se envia información de authenticación de la transacción
                String POST_PARAMS_AUTHENTICATE = objectMapper.writeValueAsString(objConcetPayment);
                String data_authenticate = CallServiceExternal.POST((Constants.URL_SERVICE_CAJACOPI + "AVGTransactionAuth/AuthenticateToken"), POST_PARAMS_AUTHENTICATE);
                ResultServiceExternalDTO resultSendDataAuthenticate = this.objectMapper.readValue(data_authenticate, ResultServiceExternalDTO.class);

                //Se valida si se realizo el envio de la información correctamente
                if (resultSendDataAuthenticate.getAuthenticate()) {
                    //Se actualiza el estado del envio y se envia la información de la transacción
                    authenticateTokenService.UpdateStatusSendData(objConcetPayment.getId(), Boolean.TRUE);
                    String POST_PARAMS_INFORMATION = objectMapper.writeValueAsString(dto);
                    String data_information = CallServiceExternal.POST((Constants.URL_SERVICE_CAJACOPI + "AVGTransaction/InformationTransaction"), POST_PARAMS_INFORMATION);
                    ResultServiceExternalDTO resultSendDataInformation = this.objectMapper.readValue(data_information, ResultServiceExternalDTO.class);

                    if (resultSendDataInformation.getTransactionSaved()) {
                        //Se actualiza estado del envio de la información
                        InformationTransactionService.updateStatusSendData(resultInsertInformation.getId(), Boolean.TRUE);
                    }
                }
            } else if(objConcetPayment.getIdConcetPayment().equals("3")){
                //Se envia información de authenticación de la transacción
                String POST_PARAMS_AUTHENTICATE = objectMapper.writeValueAsString(objConcetPayment);
                String data_authenticate = CallServiceExternal.POST((Constants.URL_SERVICE_CAJACOPI_RESBAR + "AVGTransactionAuth/AuthenticateToken"), POST_PARAMS_AUTHENTICATE);
                ResultServiceExternalDTO resultSendDataAuthenticate = this.objectMapper.readValue(data_authenticate, ResultServiceExternalDTO.class);

                //Se valida si se realizo el envio de la información correctamente
                if (resultSendDataAuthenticate.getAuthenticate()) {
                    //Se actualiza el estado del envio y se envia la información de la transacción
                    authenticateTokenService.UpdateStatusSendData(objConcetPayment.getId(), Boolean.TRUE);
                    String POST_PARAMS_INFORMATION = objectMapper.writeValueAsString(dto);
                    String data_information = CallServiceExternal.POST((Constants.URL_SERVICE_CAJACOPI_RESBAR + "AVGTransaction/InformationTransaction"), POST_PARAMS_INFORMATION);
                    ResultServiceExternalDTO resultSendDataInformation = this.objectMapper.readValue(data_information, ResultServiceExternalDTO.class);

                    if (resultSendDataInformation.getTransactionSaved()) {
                        //Se actualiza estado del envio de la información
                        InformationTransactionService.updateStatusSendData(resultInsertInformation.getId(), Boolean.TRUE);
                    }
                }
            }
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, (Supplier<String>) ex);
            ObjResult = new ResultDTO("Error al guardar información de autenticación.", 1, Boolean.FALSE);
        }
        return ObjResult;
    }
    
    
    
}
