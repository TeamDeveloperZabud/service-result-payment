/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.InformationTransaction.resource.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author DADUIS
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE, creatorVisibility = JsonAutoDetect.Visibility.NONE)
public class ResultDTO {

    @JsonProperty("Error")
    private String Error;
    @JsonProperty("CodeError")
    private int CodeError;
    @JsonProperty("TransactionSaved")
    private Boolean TransactionSaved;

    public ResultDTO(String Error, int CodeError, Boolean TransactionSaved) {
        this.Error = Error;
        this.CodeError = CodeError;
        this.TransactionSaved = TransactionSaved;
    }

    public String getError() {
        return Error;
    }

    public void setError(String Error) {
        this.Error = Error;
    }

    public int getCodeError() {
        return CodeError;
    }

    public void setCodeError(int CodeError) {
        this.CodeError = CodeError;
    }

    public Boolean getTransactionSaved() {
        return TransactionSaved;
    }

    public void setTransactionSaved(Boolean TransactionSaved) {
        this.TransactionSaved = TransactionSaved;
    }

}
