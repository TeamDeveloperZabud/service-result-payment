/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.InformationTransaction.service;

import com.example.msp.model.InformationTransaction.model.InformationTransaction;
import com.example.msp.model.InformationTransaction.model.InformationTransactionRepository;
import com.example.msp.model.InformationTransaction.resource.dto.InformationTransactionDTO;
import com.example.msp.utilidades.ObjectMapperUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author DADUIS
 */
@Service
public class InformationTransactionServiceImpl implements InformationTransactionService {

    @Autowired
    private InformationTransactionRepository InformationTransactionRepository;
    ObjectMapper objectMapper = ObjectMapperUtil.getInstanceObjectMapper();

    @Override
    public InformationTransaction guardarInformationTransaction(InformationTransactionDTO dto) {
        try {

            Date fechaBanco = new SimpleDateFormat("dd-MM-yyyy").parse(dto.getBankProcessDate());
            Date fechaSolicitado = new SimpleDateFormat("dd-MM-yyyy").parse(dto.getSoliciteDate());

            final Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            
            cal.set(Calendar.MONTH, fechaBanco.getMonth());
            cal.set(Calendar.YEAR, fechaBanco.getYear());
            cal.set(Calendar.DAY_OF_WEEK, fechaBanco.getDay());

            fechaBanco = cal.getTime();

            cal.set(Calendar.MONTH, fechaSolicitado.getMonth());
            cal.set(Calendar.YEAR, fechaSolicitado.getYear());
            cal.set(Calendar.DAY_OF_WEEK, fechaSolicitado.getDay());

            fechaSolicitado = cal.getTime();

            return InformationTransactionRepository.save(new InformationTransaction(dto.getId(),
                    dto.getUserAVG(),
                    dto.getPasswordAVG(),
                    dto.getCus(),
                    dto.getStatusTransaction(),
                    fechaBanco,
                    fechaSolicitado,
                    dto.getClientUrl(),
                    dto.getTransactionCycle(),
                    dto.getVatValue(),
                    dto.getAmount(),
                    dto.getInvoiceDescription(),
                    dto.getBankName(),
                    dto.getSendData()));
        } catch (ParseException ex) {
            Logger.getLogger(InformationTransactionServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void updateStatusTransaction(Long id, int state) {
        InformationTransactionRepository.UpdateStatusTransaction(id, state);
    }

    @Override
    public Long GetOrderToUpdate(Long Cus) {
        return InformationTransactionRepository.GetOrderToUpdate(Cus);
    }

    @Override
    public void updateStatusSendData(Long id, Boolean state) {
        InformationTransactionRepository.UpdateStatusSendData(id, state);
    }

    @Override
    public List<InformationTransaction> findBySendData() {
        return InformationTransactionRepository.findBySendData();
    }

}
