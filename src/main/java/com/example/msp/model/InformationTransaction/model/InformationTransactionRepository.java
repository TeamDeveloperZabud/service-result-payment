/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.InformationTransaction.model;

import java.util.List;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author DADUIS
 */
public interface InformationTransactionRepository extends CrudRepository<InformationTransaction, Long> {

    @Transactional
    @Modifying
    @Query(value = "update ps_orders set current_state = :State  where id_order = :ID", nativeQuery = true)
    void UpdateStatusTransaction(@Param("ID") Long id, @Param("State") int state);

    @Query(value = "select o.id_order  from ps_orders o inner join ps_authenticate_token aut on aut.id_ticket = o.id_cod_pa where aut.cus = :Cus limit 1", nativeQuery = true)
    Long GetOrderToUpdate(@Param("Cus") Long cus);

    @Transactional
    @Modifying
    @Query(value = "update ps_information_transation set send_data = :State  where id = :ID", nativeQuery = true)
    void UpdateStatusSendData(@Param("ID") Long id, @Param("State") Boolean state);

    @Transactional
    @Query(value = "SELECT * FROM ps_information_transation WHERE send_data <> TRUE;", nativeQuery = true)
    List<InformationTransaction> findBySendData();
}
