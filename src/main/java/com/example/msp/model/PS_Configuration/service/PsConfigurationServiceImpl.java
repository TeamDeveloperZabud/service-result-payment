/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.PS_Configuration.service;

import com.example.msp.model.PS_Configuration.model.PsConfiguration;
import com.example.msp.model.PS_Configuration.model.PsConfigurationRepository;
import com.example.msp.utilidades.ObjectMapperUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author DADUIS
 */
@Service
public class PsConfigurationServiceImpl implements PsConfigurationService {

    @Autowired
    private PsConfigurationRepository psConfigurationRepository;
    ObjectMapper objectMapper = ObjectMapperUtil.getInstanceObjectMapper();

    @Override
    public List<PsConfiguration> findConfigurationPSE() {
        return psConfigurationRepository.findByNameInPSE();
    }

    @Override
    public List<PsConfiguration> findConfigurationTC() {
        return psConfigurationRepository.findByNameInTC();
    }

}
