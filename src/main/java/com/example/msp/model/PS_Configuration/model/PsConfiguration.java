/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.PS_Configuration.model;

import com.example.msp.model.InformationTransaction.model.*;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

/**
 *
 * @author DADUIS
 */
@Entity
@Table(name = "ps_configuration")
public class PsConfiguration {
    
    @GeneratedValue(strategy = GenerationType.AUTO)
    @javax.persistence.Id
    private Long id_configuration;
    private Long idShopGroup;
    private Long idShop;
    private String name;
    private String value;
    private Date dateAdd;
    private Date dateUpd;

    public PsConfiguration() {
    }

    public PsConfiguration(Long id_configuration, Long idShopGroup, Long idShop, String name, String value, Date dateAdd, Date dateUpd) {
        this.id_configuration = id_configuration;
        this.idShopGroup = idShopGroup;
        this.idShop = idShop;
        this.name = name;
        this.value = value;
        this.dateAdd = dateAdd;
        this.dateUpd = dateUpd;
    }

    public Long getId_configuration() {
        return id_configuration;
    }

    public void setId_configuration(Long id_configuration) {
        this.id_configuration = id_configuration;
    }

    public Long getIdShopGroup() {
        return idShopGroup;
    }

    public void setIdShopGroup(Long idShopGroup) {
        this.idShopGroup = idShopGroup;
    }

    public Long getIdShop() {
        return idShop;
    }

    public void setIdShop(Long idShop) {
        this.idShop = idShop;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getDateAdd() {
        return dateAdd;
    }

    public void setDateAdd(Date dateAdd) {
        this.dateAdd = dateAdd;
    }

    public Date getDateUpd() {
        return dateUpd;
    }

    public void setDateUpd(Date dateUpd) {
        this.dateUpd = dateUpd;
    }

  
    
    
    
}
