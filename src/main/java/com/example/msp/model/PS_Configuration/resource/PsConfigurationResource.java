/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.PS_Configuration.resource;

import com.example.msp.model.PS_Configuration.model.PsConfiguration;
import com.example.msp.utilidades.ObjectMapperUtil;
import com.example.msp.model.PS_Configuration.service.PsConfigurationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author DADUIS
 */
@RestController
@RequestMapping("/PsConfiguration")
public class PsConfigurationResource {

    @Autowired
    private PsConfigurationService psConfigurationService;
    ObjectMapper objectMapper = ObjectMapperUtil.getInstanceObjectMapper();

    @RequestMapping(value = "configurationPSE", method = RequestMethod.GET)
    public List<PsConfiguration> findPSE() {
        return psConfigurationService.findConfigurationPSE();
    }

    @RequestMapping(value = "configurationTC", method = RequestMethod.GET)
    public List<PsConfiguration> findTC() {
        return psConfigurationService.findConfigurationTC();
    }

    @RequestMapping(value = "auth", method = RequestMethod.GET)
    public List<PsConfiguration> autentication() {
        return psConfigurationService.findConfigurationTC();
    }

}
