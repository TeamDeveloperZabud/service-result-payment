/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.msp.model.PS_Configuration.model;

import java.util.List;
import org.hibernate.annotations.Nationalized;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author DADUIS
 */
public interface PsConfigurationRepository extends CrudRepository<PsConfiguration, Long> {

    @Nationalized
    @Query(value = "select * from ps_configuration where name in ('VGMPTIEMPO_URL_PSE_AUTHENTICATE','VGMPTIEMPO_PSE_USER','VGMPTIEMPO_PSE_PASSWORD')", nativeQuery = true)
    List<PsConfiguration> findByNameInPSE();

    @Nationalized
    @Query(value = "select * from ps_configuration where name in ('VGMPTIEMPO_URL_CARD_TOKEN','VGMPTIEMPO_URL_CARD_AUTHENTICATE','VGMPTIEMPO_ENDPOINT_USER','VGMPTIEMPO_ENDPOINT_PASSWORD')", nativeQuery = true)
    List<PsConfiguration> findByNameInTC();
}
